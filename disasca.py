import numpy as np
import pandas as pd
import argparse
import csv

"""
disASCA :	Dissimilarity Anova Simultanout Component Analysis

A tool for resolving similarities based on a dissimilarity matrix into its design factors

To Run: disasca.py -i <Similarity Matrix.txt> -d <Design Matrix.txt> -o <Ordination Components.txt> -df <'label in Design of interest'> -dc <'Label in Design corrected for'>
"""

# Setup arguments parsed from commandline
parser = argparse.ArgumentParser()
parser.add_argument('-input','-i',help='Symmetric Distance matrix (with sample names in first coloumn and row)',metavar="<input>")
parser.add_argument('-design','-d',help='Design matrix (with labels in first coloumn)',metavar="<design>")
parser.add_argument('-output','-o',help='outputfile',metavar="<output>")
parser.add_argument('-designfocus','-df',help='The factor in the design which is focused on',metavar="<d1>")
parser.add_argument('-designcov','-dc',help='The factor in the design which is covariates',metavar="<d2>")
args = parser.parse_args()

### Read in data and design
#data = np.genfromtxt('unweighted_unifrac_rarefaction_139000_0.txt',delimiter='\t')
#design = pd.read_csv("mapping_STH.txt",delim_whitespace=True,skipinitialspace=True)
data = np.genfromtxt(args.input,delimiter='\t',dtype=None)
rowlabels = data[1:,0]
collabels = data[0,1:]
design = pd.read_csv(args.design,delim_whitespace=True,skipinitialspace=True)

# extract design factors
d1 = design[args.designfocus][0:]
d2 = design[args.designcov][0:]

D1 = pd.get_dummies(d1)
D2 = pd.get_dummies(d2)

X = np.matrix(data[1:,1:][:,:],dtype=float)

# Preprocess data by double centering
X = np.exp(-X)
n = np.shape(X)[1]
P = (np.identity(n) - np.ones((n,n))/n)
X = P*X*P 

SStot = np.trace(X*X)
print " "
print "**** DISASCA ****"
print "Performing Variation Splitting"
print "Total Variation (centered data) is %.5r" % SStot

# calculate marginal D1
invD2 = np.linalg.pinv(D2) 
mD = np.dot((np.identity(n) - np.dot(D2,invD2)),D1)

# calculate H
H = np.dot(mD,np.linalg.pinv(mD))

# calculate Xhat(D)
Xd = H*X*H;
SSd1marg = np.trace(Xd*Xd)

# calculate E
DD = np.concatenate((D1,D2),axis=1)
Hall = np.dot(DD,np.linalg.pinv(DD))
E = X - Hall*X*Hall; 

SSe = np.trace(E*E)
print "Marginal Variation related to Design factor %s is %.5r" % (args.designfocus, SSd1marg)
print "Residual Variation is %.5r" % SSe

## calculate basis vector from Xd + E
#U, s, V = np.linalg.svd(Xd + E, full_matrices=False)
#np.savetxt(args.output,U)

Xtrim = pd.DataFrame(Xd+E, index=rowlabels, columns=collabels)

# write trimed matrix to file. 
Xtrim.to_csv(args.output, index=True, header=True,sep='\t')

print np.shape(E)